#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/resource.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define QLEN  32
#define BUFSIZE 50000
#define ALLUSER 30

extern int errno;
u_short	portbase = 0;

struct sarray{
	char *arg[100];
	int endnum;
	int inpipeuse;
	int size;
	int pipefd[2];
	int onlyPrintFirstUnknownCmd;
};
struct tab{
	int connect;
	int pipefd[2];
};
struct user{
	int id;
	int pipefd[2];
	int fd;
	char *IP;
	int port;
	char *name;
	char *env;
	struct tab table[5000];
	int tabnum;
	int countline;		
	int onlyPrintFirstUnknownCmd;
};

void reaper(int sig);
int TCPpassing(int sockfd,fd_set *afds);
int passiveTCP(char *service,int qlen);
int passivesock(char *,char *,int);
int readline(int fd,char *ptr,int maxlen);
int writefile(char *filename);
int token(char *,int,int,struct tab *,int *tabnum,int me,fd_set *afds);
void createpipe(int k,struct sarray *stra);
int searchtable(int countline,struct tab *table);
int updatetable(int check,int *tabnum,struct tab *table);
int setusertable(int fd,struct sockaddr_in fsin);
int searchusertable(int fd);
void delusertable(int,fd_set *afds);
void createuserpipe(int);
void broadcast(char *,struct user,int flag,int rpnum);//flag=0:not cmd 1:yell 2:pipein 3:pipeout 4:name
void unicast(char *,int fd,struct user,int uid);
struct tab usetable(int index,struct tab *table);
//int countline;
int pipecount=0;
int unknown = 0;
struct user uarray[ALLUSER];
int unum=0;
char *delin=" \n\r";

main(int argc,char *argv[])
{
	char *service="12300";
	struct sockaddr_in fsin;
	int alen;
	int msock;
	int ssock;
	int fd;
	int me;
	int nfds;
	int i;
	fd_set rfds;
	fd_set afds;
	switch(argc){
	case 1:
		break;
	case 2:
		service=argv[1];
		break;
	default:
		printf("usage:no port");
		exit(1);
	}
	msock=passiveTCP(service,QLEN);
	chdir("/u/gcs/104/0456128/project2/single/ras/");
	setenv("PATH","bin:.",1);
	//signal(SIGCHLD,reaper);
	nfds=getdtablesize();
	FD_ZERO(&afds);
	FD_SET(msock,&afds);
	printf("socket success\n");
	
	while(1){
		memcpy(&rfds,&afds,sizeof(rfds));
		if(select(nfds,&rfds,(fd_set*)0,(fd_set*)0,(struct timeval *)0)<0){
			printf("select error\n");
			exit(0);
		}
		if(FD_ISSET(msock,&rfds)){
		
			alen=sizeof(fsin);
			//printf("fuck\n");
			ssock=accept(msock,(struct sockaddr *)&fsin,&alen);
			//printf("fuck1\n");
			//printf("clientIP:%s\n",inet_ntoa(fsin.sin_addr));
			if(ssock<0){
				printf("accept error\n");
				exit(1);
			}
			FD_SET(ssock,&afds);
			me=setusertable(ssock,fsin);
			if(me>=0){			
				write(ssock,"****************************************\n** Welcome to the information server. **\n****************************************  \n",125);
				broadcast("join",uarray[me],0,0);
				write(ssock,"% ",2);
			}
		}
		for(i=0;i<ALLUSER; ++i){
			if(uarray[i].fd!=0){
				if(uarray[i].fd !=msock && FD_ISSET(uarray[i].fd,&rfds)){
					setenv("PATH",uarray[i].env,1);
					TCPpassing(uarray[i].fd,&afds);
				}
			}
		}
	}
}
int TCPpassing(int sockfd,fd_set *afds)
{
	int n;
	int me;
	char line[BUFSIZE];
	//struct tab table[5000];
	//int tabnum=0;
	//int countline=0;
	printf("ready\n");
	me=searchusertable(sockfd);
	//for(;;){
		bzero(line,BUFSIZE);
		n=readline(sockfd,line,BUFSIZE);
		uarray[me].countline++;
		if (unknown==1)
		{
			uarray[me].countline--;
			unknown = 0;
		}
		if(n==0) return 0;
		else if(n<0){ //errexit("readline error");
			printf("readline error\n");
			exit(1);
		}
		//printf("to token\n");
		token(line,sockfd,uarray[me].countline,uarray[me].table,&(uarray[me].tabnum),me,afds);
		write(sockfd,"% ",2);
	//}
	return 0;
}

void reaper(int sig)
{
        int status;
        while(wait3(&status, WNOHANG, (struct rusage *)0) >= 0);
}
int passiveTCP(char *service,int qlen)
{
	return passivesock(service,"TCP",qlen);
}
int passivesock(char *service,char *protocal,int qlen)
{
	struct servent *pse;
	struct protoent *ppe;
	struct sockaddr_in sin;
	int sockfd,type;
	
	
	bzero((char *)&sin,sizeof(sin));
	sin.sin_family=AF_INET;
	sin.sin_addr.s_addr=htonl(INADDR_ANY);
	if(pse=getservbyname(service,protocal))
		sin.sin_port=htons(ntohs((u_short)pse->s_port)+portbase);
	else if((sin.sin_port=htons((u_short)atoi(service)))==0){
		printf("port error\n");
		exit(1);
	}
	if((ppe=getprotobyname(protocal))==0){
		printf("portname error\n");
		exit(1);
	}
	if(strcmp(protocal,"udp")==0)
		type=SOCK_DGRAM;
	else
		type=SOCK_STREAM;
	printf("port:%d\n",ntohs(sin.sin_port));
	sockfd=socket(PF_INET,type,ppe->p_proto);
	if(sockfd<0){
		//errexit("usage:connect server error");
		printf("socket fail\n");
		exit(1);
	}
	if(bind(sockfd,(struct sockaddr *)&sin,sizeof(sin))<0){
		//errexit("usage:connect server error");
		printf("bind fail\n");
		exit(1);
	}
	if(type==SOCK_STREAM&&listen(sockfd,qlen)<0){
		//errexit("usage:connect server error");
		printf("listen fail\n");
		exit(1);
	}
	printf("rerurn sockfd\n");
	return sockfd;
}

int token(char *s,int fd,int countline,struct tab *table,int *tabnum,int me,fd_set *afds)
{
	long i=0;
	char *a[6000];
	char *temp;
	char *pipenum;
	int uidex;
	char *mes;
	int pid,pid1;
	int k,l,r,f,t;
	int segcount=0;
	int count=0;
	int check;
	int uid;
	//int nfds;
	struct sarray stra[7000];
	int uptateindex=0;
	struct tab entry;
	struct tab entry1;
	struct user utemp;
	int inpipe=0;
	int outpipe=0;
	int inpipeerr=0;
	int outpipeerr=0;
	char *cmdline;
	char *cmdlinetemp;
	bzero(stra,7000*sizeof(struct sarray));
	//nfds=getdtablesize();
	//setenv("PATH",uarray[me].env,1);
	cmdline=(char *)malloc(BUFSIZE);
	strcpy(cmdline,s);
	cmdlinetemp=strtok(cmdline,"\n\r");
	cmdline=NULL;
	uarray[me].onlyPrintFirstUnknownCmd = 0;
	temp=strtok(s,delin);
	if(strcmp(temp,"name")==0){
		mes=strtok(NULL,"\n\r");
		if(mes==NULL){
			uarray[me].name=NULL;
		}
		else{
			uarray[me].name=NULL;
			uarray[me].name=(char *)malloc(30*sizeof(char));
			strcpy(uarray[me].name,mes);
		}
		broadcast("name",uarray[me],4,0);
	}
	else if(strcmp(temp,"who")==0){
		unicast("who",uarray[me].fd,uarray[me],0);
	}
	else if(strcmp(temp,"yell")==0){
		mes=strtok(NULL,"\n\r");
		broadcast(mes,uarray[me],1,0);
	}
	else if(strcmp(temp,"tell")==0){
		temp=strtok(NULL,delin);
		uid=atoi(temp);
		mes=strtok(NULL,"\n\r");
		uidex=uid-1;
		if(uarray[uidex].fd==0){
			unicast("tell error",uarray[me].fd,uarray[me],uid);
		}
		else unicast(mes,uarray[uidex].fd,uarray[me],0);
	}
	else{
	
		while(temp!=NULL)
		{
			a[i]=temp;
			temp=strtok(NULL,delin);
			i++;
		}

		if(strcmp(a[0],"exit")==0){		
			broadcast("exit",uarray[me],0,0);		
			delusertable(me,afds);
			//exit(0);
		}
		else if(strcmp(a[0],"setenv")==0){
			setenv(a[1],a[2],1);
			strcpy(uarray[me].env,a[2]);
		}
		else if(strcmp(a[0],"printenv")==0){
			if(fork()==0){
				close(1);
				dup2(fd,1);
				printf("%s=%s\n",a[1],getenv(a[1]));
				fflush(stdout);
				exit(0);
			}
			else	
				wait(NULL);
		}
		else if((strcmp(a[0],"cat")==0)|| (strcmp(a[0],"ls")==0)|| (strcmp(a[0],"removetag")==0)||
			(strcmp(a[0],"removetag0")==0)|| (strcmp(a[0],"noop")==0)|| (strcmp(a[0],"number")==0)||(strcmp(a[0],"delayedremovetag")==0)){
			for(k=l=t=0;k<i;k++,l++,t++)
			{
				if((*(a[k])!='>') && (*(a[k])!='|') && (*(a[k])!='!')&& (*(a[k])!='<')){
					count++;
					stra[segcount].arg[l]=a[k];
					stra[segcount].size=count;
					stra[segcount].endnum=0;
					if(k==(i-1)){
						stra[segcount].arg[l+1]=0;
					}
				}
				else{
					stra[segcount].endnum=k;
					stra[segcount].arg[l]=0;
					if((*(a[k])=='<')&&(strlen(a[k])>1)){
						stra[segcount].inpipeuse=k+1;
						k++;
						if(k>(i-1)){
							stra[segcount].inpipeuse=0;
						}
						else if(k<(i-1)){
							segcount++;
							l=-1;
							count=0;
						}
					}
					else if(strlen(a[t])==1){
						segcount++;
						l=-1;
						count=0;
					}
				t=k;		
				}
			}
			if((((*(a[stra[segcount].endnum])=='|')||(*(a[stra[segcount].endnum])=='!'))&&(strlen(a[stra[segcount].endnum])>1))||(((*(a[stra[segcount].inpipeuse])=='|')||(*(a[stra[segcount].inpipeuse])=='!'))&&(strlen(a[stra[segcount].inpipeuse])>1))){
			
				if((*(a[stra[segcount].endnum])=='|')){
					pipenum=strtok(a[stra[segcount].endnum],"|");
				}
				else if((*(a[stra[segcount].endnum])=='!')){ 
					pipenum=strtok(a[stra[segcount].endnum],"!");
				}
				else if((*(a[stra[segcount].inpipeuse])=='|')){ 
					pipenum=strtok(a[stra[segcount].inpipeuse],"|");
				}
				else if((*(a[stra[segcount].inpipeuse])=='!')){ 
					pipenum=strtok(a[stra[segcount].inpipeuse],"!");
				}
				check=countline+atoi(pipenum);
				if((f=searchtable(check,table))<0){
					uptateindex=updatetable(check,tabnum,table);
					entry=usetable(uptateindex,table);
				}
				else{ entry=usetable(f,table);}
			}
			if((*(a[stra[0].endnum])=='<') && (strlen(a[stra[0].endnum])>1)){
				inpipe=1;
				pipenum=strtok(a[stra[0].endnum],"<");
				uid=atoi(pipenum);
				uidex=uid-1;
				utemp=uarray[uidex];
				if(utemp.pipefd[1]!=0){//若管線未建立需顯示訊息
					uarray[uidex].pipefd[1]=0;
				}
				else{
					inpipe=0;
					inpipeerr=1;
					unicast("no pipe",uarray[me].fd,utemp,uid);
					//return 0;
				}	
			}
			if(((*(a[stra[segcount].endnum])=='>') && (strlen(a[stra[segcount].endnum])>1))||((*(a[stra[segcount].inpipeuse])=='>') && (strlen(a[stra[segcount].inpipeuse])>1))){
				outpipe=1;
				if(uarray[me].pipefd[1]==0){
					createuserpipe(me);
				}
				else {
					outpipe=0;
					outpipeerr=1;
					unicast("inpipe error",uarray[me].fd,uarray[me],0);//管線已有東西要拒絕
					//return 0;
				}		
			}	
			r=searchtable(countline,table);
			if(r>=0){
				entry1=usetable(r,table);
				close(entry1.pipefd[1]);	
			}
			if(inpipe==1){
				close(utemp.pipefd[1]);
			}
			if((pid1=fork())<0){perror("Fork_error!\n");}
			else if(pid1==0){
				if(r>=0){
					close(0);
					dup2(entry1.pipefd[0],0);
					close(entry1.pipefd[1]);
					close(entry1.pipefd[0]);
				}
				else if(inpipe==1){
					close(0);
					dup2(utemp.pipefd[0],0);
					close(utemp.pipefd[1]);
					close(utemp.pipefd[0]);				
				}		
				for(k=0;k<=segcount;k++){
					if(inpipeerr==1){
						k++;
						if(k>segcount) break;
						inpipeerr=0;
					}
					if(((*(a[stra[k].endnum])=='|') && (strlen(a[stra[k].endnum])==1))||((*(a[stra[k].inpipeuse])=='|') && (strlen(a[stra[k].inpipeuse])==1))){
						createpipe(k,stra);
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(stra[k].pipefd[1],1);
							close(2);
							dup2(fd,2);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
							}
						}
						else{
							close(0);
							dup2(stra[k].pipefd[0],0);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
						}
					}
					else if(((*(a[stra[k].endnum])=='!') && (strlen(a[stra[k].endnum])==1))||((*(a[stra[k].inpipeuse])=='!') && (strlen(a[stra[k].inpipeuse])==1))){
						createpipe(k,stra);
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(stra[k].pipefd[1],1);
							close(2);
							dup2(stra[k].pipefd[1],2);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
							}
						}
						else{
							close(0);
							dup2(stra[k].pipefd[0],0);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
						}
					}
					else if(((*(a[stra[k].endnum])=='|') && (strlen(a[stra[k].endnum])>1))||((*(a[stra[k].inpipeuse])=='|') && (strlen(a[stra[k].inpipeuse])>1))){
						if((pid=fork())<0){
						perror("Fork_error!\n");
						exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(entry.pipefd[1],1);
							close(2);
							dup2(fd,2);
							close(entry.pipefd[1]);
							close(entry.pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
							}
						}
						else{
							wait(NULL);
						}					
					}
					else if(((*(a[stra[k].endnum])=='!') && (strlen(a[stra[k].endnum])>1))||((*(a[stra[k].inpipeuse])=='!') && (strlen(a[stra[k].inpipeuse])>1))){
						if((pid=fork())<0){
						perror("Fork_error!\n");
						exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(entry.pipefd[1],1);
							close(2);
							dup2(entry.pipefd[1],2);
							close(entry.pipefd[1]);
							close(entry.pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
							}
						}
						else{
							wait(NULL);
						}
					}
					else if(((*(a[stra[k].endnum])=='>') && (strlen(a[stra[k].endnum])==1))||((*(a[stra[k].inpipeuse])=='>') && (strlen(a[stra[k].inpipeuse])==1))){
						createpipe(k,stra);
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(stra[k].pipefd[1],1);
							close(2);
							dup2(fd,2);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
							}
						}
						else{
							close(0);
							dup2(stra[k].pipefd[0],0);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
							writefile(stra[k+1].arg[0]);
							k++;
						}			
					}
					else if(((*(a[stra[k].endnum])=='>') && (strlen(a[stra[k].endnum])>1))||((*(a[stra[k].inpipeuse])=='>') && (strlen(a[stra[k].inpipeuse])>1))){
						//printf("outpipestart\n");
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							//printf("uarray[me].pipefd[1]=%d..................\n",uarray[me].pipefd[1]);
							//printf("uarray[me].pipefd[0]=%d..................\n",uarray[me].pipefd[0]);
							if((*(a[stra[k].endnum]+1)=='|')||*(a[stra[k].inpipeuse]+1)=='|'){
								if(outpipeerr==0){
									close(1);
									dup2(uarray[me].pipefd[1],1);
									close(2);
									dup2(fd,2);
									close(uarray[me].pipefd[1]);
									close(uarray[me].pipefd[0]);
								}
								if(execvp(stra[k].arg[0],stra[k].arg)){
									close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
								}
							}
							else if((*(a[stra[k].endnum]+1)=='!')||*(a[stra[k].inpipeuse]+1)=='!'){
								if(outpipeerr==0){
									close(1);
									dup2(uarray[me].pipefd[1],1);
									close(2);
									dup2(uarray[me].pipefd[1],2);
									close(uarray[me].pipefd[1]);
									close(uarray[me].pipefd[0]);
								}
								if(execvp(stra[k].arg[0],stra[k].arg)){
									close(1);
									if(uarray[me].onlyPrintFirstUnknownCmd==0)
									{
										uarray[me].onlyPrintFirstUnknownCmd = 1;
										dup2(fd, 1);
										printf("Unknown command: [%s].\n",stra[k].arg[0]);
										fflush(stdout);
									}
									exit(0);
								}						
							}
						}
						else{
							wait(NULL);
							if(outpipe==1){
								broadcast(cmdlinetemp,uarray[me],3,0);
							}
						}	
					}
					else{
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(fd,1);
							close(2);
							dup2(fd,2);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								if(uarray[me].onlyPrintFirstUnknownCmd==0)
								{
									uarray[me].onlyPrintFirstUnknownCmd = 1;
									dup2(fd, 1);
									printf("Unknown command: [%s].\n",stra[k].arg[0]);
									fflush(stdout);
								}
								exit(0);
							}		
						}
						else{
							wait(NULL);
						}			
					}				
				}	
				exit(0);
			}
			else{
				wait(NULL);
				if(r>=0){
					close(entry1.pipefd[0]);
				}
				else if(inpipe==1){
					close(utemp.pipefd[0]);	
					if((utemp.id!=uarray[me].id)||(outpipe==0)){
						uarray[uidex].pipefd[0]=0;
					}
					broadcast(cmdlinetemp,uarray[me],2,utemp.id);
				}
			}	
		}
		else{
			unknown=1;
			if(fork()==0){
				close(1);
				dup(fd);
				printf("Unknown command: [%s].\n",stra[k].arg[0]);
				fflush(stdout);
				exit(0);
			}
			else	
				wait(NULL);
		}
	}
	return 0;
}
void broadcast(char *mes,struct user u,int flag,int rpnum)
{
	int i=0;
	if(fork()==0){
		if(strcmp(mes,"exit")==0 && flag==0){
		//printf("*** User '%s' left. ***\n",u.name);
			for(i=0;i<ALLUSER; ++i){
				if((uarray[i].fd!=0) && (uarray[i].fd!=u.fd)){
					close(1);
					dup2(uarray[i].fd,1);
					printf("*** User '%s' left. ***\n",u.name);
				}
			}
		}
		else if(strcmp(mes,"join")==0 && flag==0){
			for(i=0;i<ALLUSER; ++i){
				if((uarray[i].fd!=0)/*&&(uarray[i].fd!=u.fd)*/){
					close(1);
					dup2(uarray[i].fd,1);
					printf("*** User '%s' entered from %s/%d. ***\n",u.name,u.IP,u.port);
				}
			}			
		}
		else if(flag==1){
			for(i=0;i<ALLUSER; ++i){
				if((uarray[i].fd!=0)/*&&(uarray[i].fd!=u.fd)*/){
					close(1);
					dup2(uarray[i].fd,1);
					printf("*** %s yelled ***:  %s\n",u.name,mes);
				}
			}			
		}
		else if(flag==2){
			for(i=0;i<ALLUSER; ++i){
				if((uarray[i].fd!=0)/*&&(uarray[i].fd!=u.fd)*/){
					close(1);
					dup2(uarray[i].fd,1);
					printf("*** %s (#%d) just received from the pipe #%d by '%s' ***\n",u.name,u.id,rpnum,mes);
				}
			}				
		}
		else if(flag==3){
			for(i=0;i<ALLUSER; ++i){
				if((uarray[i].fd!=0)/*&&(uarray[i].fd!=u.fd)*/){
					close(1);
					dup2(uarray[i].fd,1);
					printf("*** %s (#%d) just piped '%s' into his/her pipe. *** \n",u.name,u.id,mes);
				}
			}	
		}
		else if(flag==4){
			//printf("*** User from %s/%d is named '%s'. ***\n",u.IP,u.port,u.name);
			for(i=0;i<ALLUSER; ++i){
				if((uarray[i].fd!=0)/*&&(uarray[i].fd!=u.fd)*/){
					close(1);
					dup2(uarray[i].fd,1);
					printf("*** User from %s/%d is named '%s'. ***\n",u.IP,u.port,u.name);
				}
			}				
		}
	exit(0);
	}
	else{
		wait(NULL);
	}
}
void unicast(char *mes,int fd,struct user u,int uid)
{
	int i;
	if(fork()==0){
		close(1);
		dup2(fd,1);
		if(strcmp(mes,"no pipe")==0){
			printf("*** Error: the pipe from #%d does not exist yet. ***\n",uid);
		}
		else if(strcmp(mes,"who")==0){
			printf("  <ID>       <nickname>            <IP/port>          <indicate me>\n");
			for(i=0;i<ALLUSER;i++){
				if((uarray[i].fd!=0)&&(uarray[i].fd!=u.fd)){
					printf("  %d       %s            %s/%d\n",uarray[i].id,uarray[i].name,uarray[i].IP,uarray[i].port);
				}
				else if((uarray[i].fd!=0)&&(uarray[i].fd==u.fd)){
					printf("  %d       %s            %s/%d          <- me\n",uarray[i].id,uarray[i].name,uarray[i].IP,uarray[i].port);
				}
			}			
		}
		else if(strcmp(mes,"inpipe error")==0){
			printf("*** Error: your pipe already exists. ***\n");
		}
		else if (strcmp(mes,"tell error")==0){
			printf("*** Error: user #<%d> does not exist yet. ***\n",uid);
		}
		else{
			printf("*** %s told you ***:  %s\n",u.name,mes);	
		}
	exit(0);
	}
	else
		wait(NULL);
}
int searchusertable(int fd)
{
	int u;
	for(u=0;u<ALLUSER;u++){
		if(uarray[u].fd==fd){
			return u;
		}
	}
	printf("usertable error\n");
	exit(0);
}
int setusertable(int fd,struct sockaddr_in fsin)
{
	int u;
	unum++;
	for(u=0;u<ALLUSER;u++){
		if(uarray[u].fd==0){
			uarray[u].name=(char *)malloc(30);
			uarray[u].IP=(char *)malloc(30);
			uarray[u].env=(char *)malloc(30);
			uarray[u].id=u+1;
			strcpy(uarray[u].name,"(no name)");
			uarray[u].fd=fd;
			uarray[u].port=ntohs(fsin.sin_port);
			//printf("client port:%d\n",ntohs(fsin.sin_port));
			//printf("client port:%d\n",uarray[u].port);
			strcpy(uarray[u].IP,inet_ntoa(fsin.sin_addr));
			//printf("clientIP:%s\n",inet_ntoa(fsin.sin_addr));
			strcpy(uarray[u].env,"bin:.");
			return u;
		}
	}
	return -1;	
}
void delusertable(int me,fd_set *afds)
{
	//int i;
	//unum--;
	//uarray[me].id=0;
	//uarray[me].name=NULL;
	close(uarray[me].fd);
	FD_CLR(uarray[me].fd,afds);
	//uarray[me].fd=0;
	//uarray[me].IP=NULL;
	//uarray[me].countline=0;
	//uarray[me].tabnum=0;
	if((uarray[me].pipefd[1]!=0)||(uarray[me].pipefd[0]!=0)){
		close(uarray[me].pipefd[1]);
		close(uarray[me].pipefd[0]);
	}
	bzero(&(uarray[me]),sizeof(struct user));
	//uarray[me].pipefd[1]=0;
	//uarray[me].pipefd[1]=0;
	//uarray[me].env=NULL;
	/*for(i=0;i<5000;++i){
		uarray[me].table[i].connect=0;
		uarray[me].table[i].pipefd[0]=0;
		uarray[me].table[i].pipefd[1]=0;
	}*/
	//free(uarray[me].name);
	//free(uarray[me].IP);
	//free(uarray[me].env);
}
void createuserpipe(int u)
{
	if(pipe(uarray[u].pipefd)<0){
		perror("pipe error");
		//exit(0);
	}
}
int searchtable(int countline,struct tab *table)
{
	int i;
	for(i=0;i<5000;i++){
		if (countline==table[i].connect){
			return i;
		}
	}
	return -1;
}
int updatetable(int check,int *tabnum,struct tab *table)
{
	int i;
	i=(*tabnum);
	table[*tabnum].connect=check;
	if(pipe(table[*tabnum].pipefd)<0){
		perror("pipe error");
		exit(0);
	}
	(*tabnum)++;
	return i;
}
struct tab usetable(int index,struct tab *table)
{
	return table[index];
}
int readline(int fd,char *ptr,int maxlen)
{
	int n,rc;
	char c;
	*ptr = 0;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;	
			if(c=='\n')  break;
		}
		else if(rc==0)
		{
			if(n==1)     return(0);
			else         break;
		}
		else
			return(-1);
	}
	return(n);
} 
void createpipe(int k,struct sarray *stra)
{
	if(pipe(stra[k].pipefd)<0){
		perror("pipe error");
		//exit(0);
	}
}
int writefile(char *filename)
{
	int numbytes;
	FILE *fp;

	char buff[BUFSIZE];

	if ( (fp = fopen(filename, "wt")) == NULL )
			perror("can't open file");

	if ((numbytes = read(STDIN_FILENO, buff, sizeof(buff))) < 0)
		perror("Error");
	fputs(buff,fp);
	fclose(fp);
	return 0;
} 
