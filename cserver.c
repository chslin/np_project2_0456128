#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/resource.h>
#include <arpa/inet.h>
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <sys/stat.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#define QLEN  32
#define BUFSIZE 50000
#define ALLUSER 30

extern int errno;
u_short	portbase = 0;
const key_t key=9945;

struct sarray{
	char *arg[100];
	int endnum;
	int inpipeuse;
	int size;
	int pipefd[2];
};
struct tab{
	int connect;
	int pipefd[2];
};
struct user{
	int id;
	int writefd;
	int port;
	int fd;
	int useFIFOwrite;
	int useFIFO;
	char IP[30];
	char name[30];
	char FIFO[20];
	//char *env;
	//struct tab table[5000];
	//int tabnum;
	char mes[1024];
	int pid;	
};
struct shareuser{
	struct user uarray[ALLUSER];
};

void reaper(int sig);
int TCPpassing(int sockfd,int pid);
int passiveTCP(char *service,int qlen);
int passivesock(char *,char *,int);
int readline(int fd,char *ptr,int maxlen);
int writefile(char *filename);
int token(char *,int,int,struct tab *,int *tabnum,int me);
void createpipe(int k,struct sarray *stra);
int searchtable(int countline,struct tab *table);
int updatetable(int check,int *tabnum,struct tab *table);
struct tab usetable(int index,struct tab *table);
int setusertable(int pid,struct sockaddr_in fsin,int fd);
int searchusertable(int pid);
void delusertable(int);
void broadcast(char *,struct user,int flag,int rpnum);//flag=0:not cmd 1:yell 2:pipein 3:pipeout 4:name
void unicast(char *,struct user*,struct user,int uid);
void recivemes(int sig);
void createuserFIFO(int);
struct shareuser *shmuser;
int countline;
int pipecount=0;
char *delin=" \n\r";

main(int argc,char *argv[])
{
	char *service="12300";
	struct sockaddr_in fsin;
	int alen;
	int msock;
	int ssock;
	int pid;
	int me;
	switch(argc){
	case 1:
		break;
	case 2:
		service=argv[1];
		break;
	default:
		printf("usage:no port");
		exit(1);
	}
	msock=passiveTCP(service,QLEN);
	chdir("/u/gcs/104/0456128/project2/concurrent/ras/");
	setenv("PATH","bin:.",1);
	//signal(SIGCHLD,SIG_IGN);
	signal(SIGCHLD,reaper);
	printf("socket success\n");
	int shmid = shmget(key, sizeof(struct shareuser), 0666 | IPC_CREAT);
    if (shmid == -1)
    {
        printf("shmget failed\r\n");
        exit(0);
    }

    void *shared_memory = shmat(shmid, NULL, 0);
    if (shared_memory == (void*)-1)
    {
        printf("shmat failed\r\n");
        exit(0);
    }
	shmuser = (struct shareuser *)shared_memory;
	bzero(shmuser,sizeof(struct shareuser));
	while(1){
		alen=sizeof(fsin);
		ssock=accept(msock,(struct sockaddr *)&fsin,&alen);
		if(ssock<0){
			if(errno==EINTR)
				continue;
			exit(1);
		}
		switch(fork()){
		case 0:
			close(msock);
			printf("child\n");
			signal(SIGUSR1,recivemes);
			//printf("signal(SIGUSR1,recivemes);\n");
			//shmuser = (struct shareuser *)shared_memory;
			pid=getpid();
			
			me=setusertable(pid,fsin,ssock);
			if(me>=0){			
				write(ssock,"****************************************\n** Welcome to the information server. **\n****************************************  \n",125);
				broadcast("join",(*shmuser).uarray[me],0,0);
				write(ssock,"% ",2);
			}
			exit(TCPpassing(ssock,pid));
		default:
			close(ssock);
			break;
		case -1:
			printf("fork error");
			exit(1);
		}
	}
}
void reaper(int sig)
{
        int status;
        while(wait3(&status, WNOHANG, (struct rusage *)0) > 0);
}
int passiveTCP(char *service,int qlen)
{
	return passivesock(service,"TCP",qlen);
}
int passivesock(char *service,char *protocal,int qlen)
{
	struct servent *pse;
	struct protoent *ppe;
	struct sockaddr_in sin;
	int sockfd,type;
	
	
	bzero((char *)&sin,sizeof(sin));
	sin.sin_family=AF_INET;
	sin.sin_addr.s_addr=htonl(INADDR_ANY);
	if(pse=getservbyname(service,protocal))
		sin.sin_port=htons(ntohs((u_short)pse->s_port)+portbase);
	else if((sin.sin_port=htons((u_short)atoi(service)))==0){
		printf("port error\n");
		exit(1);
	}
	if((ppe=getprotobyname(protocal))==0){
		printf("portname error\n");
		exit(1);
	}
	if(strcmp(protocal,"udp")==0)
		type=SOCK_DGRAM;
	else
		type=SOCK_STREAM;
	printf("port:%d\n",ntohs(sin.sin_port));
	sockfd=socket(PF_INET,type,ppe->p_proto);
	if(sockfd<0){
		//errexit("usage:connect server error");
		printf("socket fail\n");
		exit(1);
	}
	if(bind(sockfd,(struct sockaddr *)&sin,sizeof(sin))<0){
		//errexit("usage:connect server error");
		printf("bind fail\n");
		exit(1);
	}
	if(type==SOCK_STREAM&&listen(sockfd,qlen)<0){
		//errexit("usage:connect server error");
		printf("listen fail\n");
		exit(1);
	}
	printf("rerurn sockfd\n");
	return sockfd;
}
int TCPpassing(int sockfd,int pid)
{
	int n;
	char line[BUFSIZE];
	struct tab table[5000];
	int tabnum=0;
	int me;
	countline=0;
	printf("ready\n");
	me=searchusertable(pid);
	for(;;){
		bzero(line,BUFSIZE);
		n=readline(sockfd,line,BUFSIZE);
		countline++;
		if(n==0) return 0;
		else if(n<0){ //errexit("readline error");
			printf("readline error\n");
			exit(1);
		}
		//printf("to token\n");
		token(line,sockfd,countline,table,&tabnum,me);
		write(sockfd,"% ",2);
	}
	return 0;
}

int token(char *s,int fd,int countline,struct tab *table,int *tabnum,int me)
{
	long i=0;
	char *a[6000];
	char *temp;
	char *pipenum;
	char *mes;
	int pid,pid1;
	int k,l,r,f,t;
	int segcount=0;
	int count=0;
	int check;
	int uid;
	int uidex;
	struct sarray stra[7000];
	int uptateindex=0;
	struct tab entry;
	struct tab entry1;
	struct user utemp;
	int inpipeerr=0;
	int outpipeerr=0;
	int inpipe=0;
	int outpipe=0;
	char *cmdline;
	char *cmdlinetemp;
	int readfd;
	bzero(stra,7000*sizeof(struct sarray));
	cmdline=(char *)malloc(BUFSIZE);
	strcpy(cmdline,s);
	cmdlinetemp=strtok(cmdline,"\n\r");
	cmdline=NULL;
	temp=strtok(s,delin);
	if(strcmp(temp,"name")==0){
		mes=strtok(NULL,"\n\r");
		//printf("NAME : mes=%s\n",mes);
		if(mes==NULL){
			bzero((*shmuser).uarray[me].name,30);
		}
		else{
			//(*shmuser).uarray[me].name=NULL;
			//(*shmuser).uarray[me].name=(char *)malloc(30*sizeof(char));
			bzero((*shmuser).uarray[me].name,30);
			strcpy((*shmuser).uarray[me].name,mes);
		}
		//printf("(*shmuser).uarray[%d].name=%s\n",me,(*shmuser).uarray[me].name);
		//printf("prebroadcast\n");
		broadcast("name",(*shmuser).uarray[me],4,0);
		//printf("end broadcast\n");
	}
	else if(strcmp(temp,"who")==0){
		unicast("who",&((*shmuser).uarray[me]),(*shmuser).uarray[me],0);
	}
	else if(strcmp(temp,"yell")==0){
		mes=strtok(NULL,"\n\r");
		broadcast(mes,(*shmuser).uarray[me],1,0);
	}
	else if(strcmp(temp,"tell")==0){
		temp=strtok(NULL,delin);
		uid=atoi(temp);
		mes=strtok(NULL,"\n\r");
		uidex=uid-1;
		if((*shmuser).uarray[uidex].pid==0){
			unicast("tell error",&((*shmuser).uarray[me]),(*shmuser).uarray[me],uid);
		}
		else unicast(mes,&((*shmuser).uarray[uidex]),(*shmuser).uarray[me],-1);
	}
	else{
		while(temp!=NULL)
		{
			a[i]=temp;
			temp=strtok(NULL,delin);
			i++;
		}

		if(strcmp(a[0],"exit")==0){
			broadcast("exit",(*shmuser).uarray[me],0,0);
			delusertable(me);
			exit(0);
		}
		if(strcmp(a[0],"setenv")==0){
			setenv(a[1],a[2],1);
		}
		else if(strcmp(a[0],"printenv")==0){
			if(fork()==0){
				close(1);
				dup2(fd,1);
				printf("%s=%s\n",a[1],getenv(a[1]));
				fflush(stdout);
				exit(0);
			}
			else	
				wait(NULL);
		}
		else{
			//printf("to packet cmd\n");
			for(k=l=t=0;k<i;k++,l++,t++)
			{
				if((*(a[k])!='>') && (*(a[k])!='|') && (*(a[k])!='!')&& (*(a[k])!='<')){
					count++;
					stra[segcount].arg[l]=a[k];
					stra[segcount].size=count;
					stra[segcount].endnum=0;
					if(k==(i-1)){
						stra[segcount].arg[l+1]=0;
					}
				}
				else{
					stra[segcount].endnum=k;
					stra[segcount].arg[l]=0;
					if((*(a[k])=='<')&&(strlen(a[k])>1)){
						stra[segcount].inpipeuse=k+1;
						k++;
						if(k>(i-1)){
							stra[segcount].inpipeuse=0;
						}
						else if(k<(i-1)){
							segcount++;
							l=-1;
							count=0;
						}
					}
					else if(strlen(a[t])==1){
						segcount++;
						l=-1;
						count=0;
					}
				t=k;		
				}
			}
			//printf("pre |number\n",check);
			//printf("a[stra[segcount].inpipeuse]=%s\n",a[stra[segcount].inpipeuse]);
			if((((*(a[stra[segcount].endnum])=='|')||(*(a[stra[segcount].endnum])=='!'))&&(strlen(a[stra[segcount].endnum])>1))||(((*(a[stra[segcount].inpipeuse])=='|')||(*(a[stra[segcount].inpipeuse])=='!'))&&(strlen(a[stra[segcount].inpipeuse])>1))){
				if((*(a[stra[segcount].endnum])=='|')){
					pipenum=strtok(a[stra[segcount].endnum],"|");
				}
				else if((*(a[stra[segcount].endnum])=='!')){ 
					pipenum=strtok(a[stra[segcount].endnum],"!");
				}
				else if((*(a[stra[segcount].inpipeuse])=='|')){ 
					pipenum=strtok(a[stra[segcount].inpipeuse],"|");
				}
				else if((*(a[stra[segcount].inpipeuse])=='!')){ 
					pipenum=strtok(a[stra[segcount].inpipeuse],"!");
				}
				check=countline+atoi(pipenum);
				//printf("check=%d\n",check);
				if((f=searchtable(check,table))<0){
					uptateindex=updatetable(check,tabnum,table);
					entry=usetable(uptateindex,table);
				}
				else{ entry=usetable(f,table);}
			}
			if((*(a[stra[0].endnum])=='<') && (strlen(a[stra[0].endnum])>1)){
			//printf("inpipe\n");
				inpipe=1;
				pipenum=strtok(a[stra[0].endnum],"<");
				uid=atoi(pipenum);
				//printf("uid=%d\n",uid);
				uidex=uid-1;
				utemp=(*shmuser).uarray[uidex];
				//printf("utemp.useFIFOwrite=%d\n",utemp.useFIFOwrite);
				if(utemp.useFIFOwrite!=0){//若管線未建立需顯示訊息
				//printf("open read\n");
					readfd=open((*shmuser).uarray[uidex].FIFO,0);
					//close(utemp.writefd);
					(*shmuser).uarray[uidex].useFIFOwrite=0;
					//(*shmuser).uarray[uidex].writefd=0;
				}
				else{
				//printf("inpipe=0\n");
					inpipe=0;
					inpipeerr=1;
					unicast("no pipe",&((*shmuser).uarray[me]),utemp,uid);
					//return 0;
				}	
			}
			if(((*(a[stra[segcount].endnum])=='>') && (strlen(a[stra[segcount].endnum])>1))||((*(a[stra[segcount].inpipeuse])=='>') && (strlen(a[stra[segcount].inpipeuse])>1))){
				if((*shmuser).uarray[me].useFIFOwrite==0){
				//printf("detect outpipe\n");
					outpipe=1;
				}
				else {
					outpipe=0;
					outpipeerr=1;
					unicast("inpipe error",&((*shmuser).uarray[me]),(*shmuser).uarray[me],0);//管線已有東西要拒絕
					//return 0;
				}		
			}
			r=searchtable(countline,table);
			if(r>=0){
				entry1=usetable(r,table);
				close(entry1.pipefd[1]);	
			}
			//printf("to exe cmd\n");
			if((pid1=fork())<0){perror("Fork_error!\n");}
			else if(pid1==0){
				if(r>=0){
					close(0);
					dup2(entry1.pipefd[0],0);
					close(entry1.pipefd[1]);
					close(entry1.pipefd[0]);
				}
				else if(inpipe==1){
				//printf("inpipe ok\n");
					close(0);
					dup2(readfd,0);
					//close(utemp.writefd);
					close(readfd);
					unlink((*shmuser).uarray[uidex].FIFO);
							
				}
				//printf("a[stra[%d].inpipeuse]=%s\n",segcount,a[stra[segcount].inpipeuse]);
				for(k=0;k<=segcount;k++){
					if(inpipeerr==1){
						k++;
						if(k>segcount) break;
						inpipeerr=0;
					}
					if(((*(a[stra[k].endnum])=='|') && (strlen(a[stra[k].endnum])==1))||((*(a[stra[k].inpipeuse])=='|') && (strlen(a[stra[k].inpipeuse])==1))){
						createpipe(k,stra);
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(stra[k].pipefd[1],1);
							close(2);
							dup2(fd,2);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup2(fd,1);
								printf("Unknown command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
						}
						else{
							close(0);
							dup2(stra[k].pipefd[0],0);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
						}
					}
					else if(((*(a[stra[k].endnum])=='!') && (strlen(a[stra[k].endnum])==1))||((*(a[stra[k].inpipeuse])=='!') && (strlen(a[stra[k].inpipeuse])==1))){
						createpipe(k,stra);
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(stra[k].pipefd[1],1);
							close(2);
							dup2(stra[k].pipefd[1],2);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup2(fd,1);
								printf("Unknown command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
						}
						else{
							close(0);
							dup2(stra[k].pipefd[0],0);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
						}
					}
					else if(((*(a[stra[k].endnum])=='|') && (strlen(a[stra[k].endnum])>1))||((*(a[stra[k].inpipeuse])=='|') && (strlen(a[stra[k].inpipeuse])>1))){
						//printf("in |number\n");
						if((pid=fork())<0){
						perror("Fork_error!\n");
						exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(entry.pipefd[1],1);
							close(2);
							dup2(fd,2);
							close(entry.pipefd[1]);
							close(entry.pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup2(fd,1);
								printf("Unknown command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
						}
						else{
							wait(NULL);
						}					
					}
					else if(((*(a[stra[k].endnum])=='!') && (strlen(a[stra[k].endnum])>1))||((*(a[stra[k].inpipeuse])=='!') && (strlen(a[stra[k].inpipeuse])>1))){
						if((pid=fork())<0){
						perror("Fork_error!\n");
						exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(entry.pipefd[1],1);
							close(2);
							dup2(entry.pipefd[1],2);
							close(entry.pipefd[1]);
							close(entry.pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup2(fd,1);
								printf("Unknown command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
						}
						else{
							wait(NULL);
						}
					}
					else if(((*(a[stra[k].endnum])=='>') && (strlen(a[stra[k].endnum])==1))||((*(a[stra[k].inpipeuse])=='>') && (strlen(a[stra[k].inpipeuse])==1))){
						createpipe(k,stra);
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							close(1);
							dup2(stra[k].pipefd[1],1);
							close(2);
							dup2(fd,2);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup2(fd,1);
								printf("Unknown command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}
						}
						else{
							close(0);
							dup2(stra[k].pipefd[0],0);
							close(stra[k].pipefd[1]);
							close(stra[k].pipefd[0]);
							wait(NULL);
							writefile(stra[k+1].arg[0]);
							k++;
						}			
					}
					else if(((*(a[stra[k].endnum])=='>') && (strlen(a[stra[k].endnum])>1))||((*(a[stra[k].inpipeuse])=='>') && (strlen(a[stra[k].inpipeuse])>1))){
						//printf("start outpipe\n");					
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
							if(outpipeerr==0){
								createuserFIFO(me);
							}
							if((*(a[stra[k].endnum]+1)=='|')||*(a[stra[k].inpipeuse]+1)=='|'){
								if(outpipeerr==0){
									close(1);
									dup2((*shmuser).uarray[me].writefd,1);
									close(2);
									dup2(fd,2);
									close((*shmuser).uarray[me].writefd);
								}
								if(execvp(stra[k].arg[0],stra[k].arg)){
									close(1);
									dup2(fd,1);
									printf("Unknown command:[%s]\n",stra[k].arg[0]);
									fflush(stdout);
									exit(0);
								}
							}
							else if((*(a[stra[k].endnum]+1)=='!')||*(a[stra[k].inpipeuse]+1)=='!'){
								if(outpipeerr==0){
									close(1);
									dup2((*shmuser).uarray[me].writefd,1);
									close(2);
									dup2((*shmuser).uarray[me].writefd,2);
									close((*shmuser).uarray[me].writefd);
								}
								if(execvp(stra[k].arg[0],stra[k].arg)){
									close(1);
									dup2(fd,1);
									printf("Unknown command:[%s]\n",stra[k].arg[0]);
									fflush(stdout);
									exit(0);
								}						
							}
						}
						else{
							if(outpipe==0){
								wait(NULL);
							}
							//printf("exe outpipe finish\n");
							if(outpipe==1){
								broadcast(cmdlinetemp,(*shmuser).uarray[me],3,0);
							}
						}	
					}
					else{
					//printf("inpipe ok to exe\n");
						if((pid=fork())<0){
							perror("Fork_error!\n");
							exit(0);
						}
						else if(pid==0){
						//printf("inpipe ok to exe\n");
							close(1);
							dup2(fd,1);
							close(2);
							dup2(fd,2);
							if(execvp(stra[k].arg[0],stra[k].arg)){
								close(1);
								dup2(fd,1);
								printf("Unknown command:[%s]\n",stra[k].arg[0]);
								fflush(stdout);
								exit(0);
							}		
						}
						else{
							wait(NULL);
							//printf("inpipe ok and exe finish\n");
						}			
					}		
				}	
			exit(0);
			}
			else{
			//printf("outpipe=%d\n",outpipe);
				//if(outpipe!=1){
				wait(NULL);
				//}
			//printf("outpipe end\n");
				if(r>=0){
					close(entry1.pipefd[0]);
				}
				else if(inpipe==1){
					close(readfd);
					/*if((utemp.id!=(*shmuser).uarray[me].id)||(outpipe==0)){
						unlink((*shmuser).uarray[uidex].FIFO);
					}*/
					broadcast(cmdlinetemp,(*shmuser).uarray[me],2,utemp.id);
				}
			}	
		}
	}
	return 0;
}
void recivemes(int sig)
{
	int me;
	int gpid;
	int fd;
	int i;
	gpid=getpid();
	//printf("recivemes\n");
	//printf("(*shmuser).uarray[0].mes=%s\n",(*shmuser).uarray[0].mes);
	//printf("gpid=%d\n",gpid);
	//printf("%d\n",(*shmuser).uarray[0].pid);
	for(i=0;i<ALLUSER; ++i){
		if(((*shmuser).uarray[i].pid!=0) && ((*shmuser).uarray[i].pid==gpid)){
			me=i;
			fd=(*shmuser).uarray[i].fd;
		}
	}
	//printf("me=%d\n",me);
	//printf("(*shmuser).uarray[%d].mes=%s\n",me,(*shmuser).uarray[me].mes);
	if(fork()==0){
		close(1);
		dup(fd);
		printf("%s\n",(*shmuser).uarray[me].mes);
		bzero((*shmuser).uarray[me].mes,1024);
		//(*shmuser).uarray[me].mes=(char *)malloc(1024);
		exit(0);
	}
	else{
		wait(NULL);
	}
}
void broadcast(char *mes,struct user u,int flag,int rpnum)
{
	int i=0;
	//printf("in broadcast\n");
	if(strcmp(mes,"exit")==0 && flag==0){
		for(i=0;i<ALLUSER; ++i){
			if(((*shmuser).uarray[i].pid!=0) && ((*shmuser).uarray[i].pid!=u.pid)){
				sprintf((*shmuser).uarray[i].mes,"*** User '%s' left. ***",u.name);
				kill((*shmuser).uarray[i].pid,SIGUSR1);
			}
		}
	}
	else if(strcmp(mes,"join")==0 && flag==0){
	//printf("detect join\n");
		for(i=0;i<ALLUSER; ++i){
			if(((*shmuser).uarray[i].pid!=0)){
				sprintf((*shmuser).uarray[i].mes,"*** User '%s' entered from %s/%d. ***",u.name,u.IP,u.port);
				//printf("send:(*shmuser).uarray[%d].pid=%d\n",i,(*shmuser).uarray[i].pid);
				kill((*shmuser).uarray[i].pid,SIGUSR1);
				//printf("(*shmuser).uarray[%d].mes=%s\n",i,(*shmuser).uarray[i].mes);
				//printf("(*shmuser).uarray[%d].pid=%d\n",i,(*shmuser).uarray[i].pid);
			}
		}
	//printf("broadcast0:(*shmuser).uarray[%d].mes=%s\n",i,(*shmuser).uarray[0].mes);		
	}
	else if(flag==1){
		for(i=0;i<ALLUSER; ++i){
			if(((*shmuser).uarray[i].pid!=0)){
				sprintf((*shmuser).uarray[i].mes,"*** %s yelled ***:  %s",u.name,mes);
				kill((*shmuser).uarray[i].pid,SIGUSR1);
			}
		}					
	}
	else if(flag==2){		
		for(i=0;i<ALLUSER; ++i){
			if(((*shmuser).uarray[i].pid!=0)){
				sprintf((*shmuser).uarray[i].mes,"*** %s (#%d) just received from the pipe #%d by '%s' ***",u.name,u.id,rpnum,mes);
				kill((*shmuser).uarray[i].pid,SIGUSR1);
			}
		}							
	}
	else if(flag==3){
	//printf("broadcast outpipe\n");
		for(i=0;i<ALLUSER; ++i){
			if(((*shmuser).uarray[i].pid!=0)){
				sprintf((*shmuser).uarray[i].mes,"*** %s (#%d) just piped '%s' into his/her pipe. *** ",u.name,u.id,mes);
				kill((*shmuser).uarray[i].pid,SIGUSR1);
			}
		}				
	}
	else if(flag==4){
	//printf("detect name\n");
		for(i=0;i<ALLUSER; ++i){
			if(((*shmuser).uarray[i].pid!=0)){
				sprintf((*shmuser).uarray[i].mes,"*** User from %s/%d is named '%s'. ***",u.IP,u.port,u.name);
				//printf("send message\n");
				kill((*shmuser).uarray[i].pid,SIGUSR1);
			}
		}
	}

}
void unicast(char *mes,struct user *goal,struct user u,int uid)
{
	int i;
	if(fork()==0){
		if(strcmp(mes,"no pipe")==0){
			close(1);
			dup2(u.fd,1);
			printf("*** Error: the pipe from #%d does not exist yet. ***\n",uid);
			//kill(pid,SIGUSR1);
		}
		else if(strcmp(mes,"who")==0){
			close(1);
			dup2(u.fd,1);
			printf("  <ID>       <nickname>            <IP/port>          <indicate me>\n");
			for(i=0;i<ALLUSER;i++){
				if(((*shmuser).uarray[i].pid!=0)&&((*shmuser).uarray[i].pid!=u.pid)){
					printf("  %d       %s            %s/%d\n",(*shmuser).uarray[i].id,(*shmuser).uarray[i].name,(*shmuser).uarray[i].IP,(*shmuser).uarray[i].port);
				}
				else if(((*shmuser).uarray[i].pid!=0)&&((*shmuser).uarray[i].pid==u.pid)){
					printf("  %d       %s            %s/%d          <- me\n",(*shmuser).uarray[i].id,(*shmuser).uarray[i].name,(*shmuser).uarray[i].IP,(*shmuser).uarray[i].port);
				}
			}			
		}
		else if(strcmp(mes,"inpipe error")==0){
			close(1);
			dup2(u.fd,1);
			printf("*** Error: your pipe already exists. ***\n");
		}
		else if (strcmp(mes,"tell error")==0){
			close(1);
			dup2(u.fd,1);
			printf("*** Error: user #<%d> does not exist yet. ***\n",uid);
		}
		/*else{
			sprintf((*goal).mes,"*** %s told you ***:  %s",(*u).name,mes);
			kill((*goal).pid,SIGUSR1);			
		}*/
	exit(0);
	}
	else{
		wait(NULL);
		if(uid<0){
			//(*goal).mes=NULL;
			//(*goal).mes=(char *)malloc(1024);
			sprintf((*goal).mes,"*** %s told you ***:  %s",u.name,mes);
			kill((*goal).pid,SIGUSR1);			
		}
	}
}
int searchusertable(int pid)
{
	int u;
	for(u=0;u<ALLUSER;u++){
		if((*shmuser).uarray[u].pid==pid){
			return u;
		}
	}
	printf("usertable error\n");
	exit(0);
}
int setusertable(int pid,struct sockaddr_in fsin,int fd)
{
	int u;
	for(u=0;u<ALLUSER;u++){
		if((*shmuser).uarray[u].pid==0){
			(*shmuser).uarray[u].id=u+1;
			strcpy((*shmuser).uarray[u].name,"(no name)");
			(*shmuser).uarray[u].pid=pid;
			(*shmuser).uarray[u].fd=fd;
			(*shmuser).uarray[u].port=ntohs(fsin.sin_port);
			//printf("client port:%d\n",ntohs(fsin.sin_port));
			//printf("client port:%d\n",uarray[u].port);
			strcpy((*shmuser).uarray[u].IP,inet_ntoa(fsin.sin_addr));
			//printf("clientIP:%s\n",inet_ntoa(fsin.sin_addr));
			//printf("setusertable0:(*shmuser).uarray[%d].pid=%d\n",u,(*shmuser).uarray[u].pid);
			return u;
		}
	//printf("print usertable\n");
	//printf("setusertable1:(*shmuser).uarray[%d].pid=%d\n",u,(*shmuser).uarray[u].pid);
	}
	return -1;	
}
void delusertable(int me)
{
	close((*shmuser).uarray[me].fd);
	if(((*shmuser).uarray[me].useFIFO!=0)){
		if(open((*shmuser).uarray[me].FIFO,0)==-1){
			perror("open read error\n");
		}
		unlink((*shmuser).uarray[me].FIFO);
	}
	bzero(&((*shmuser).uarray[me]),sizeof(struct user));
}
int searchtable(int countline,struct tab *table)
{
	int i;
	for(i=0;i<5000;i++){
		if (countline==table[i].connect){
			return i;
		}
	}
	return -1;
}
void createuserFIFO(int me)
{
	sprintf((*shmuser).uarray[me].FIFO,"%d", (*shmuser).uarray[me].id);
	mkfifo((*shmuser).uarray[me].FIFO,0666);
	(*shmuser).uarray[me].useFIFO=1; 
	(*shmuser).uarray[me].useFIFOwrite=1; 
	if(((*shmuser).uarray[me].writefd = open((*shmuser).uarray[me].FIFO,1))==-1){
		perror("FIFO open error\n");
		exit(1);
	}
	
}
int updatetable(int check,int *tabnum,struct tab *table)
{
	int i;
	i=(*tabnum);
	table[*tabnum].connect=check;
	if(pipe(table[*tabnum].pipefd)<0){
		perror("pipe error");
		exit(0);
	}
	(*tabnum)++;
	return i;
}
struct tab usetable(int index,struct tab *table)
{
	return table[index];
}
int readline(int fd,char *ptr,int maxlen)
{
	int n,rc;
	char c;
	*ptr = 0;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;	
			if(c=='\n')  break;
		}
		else if(rc==0)
		{
			if(n==1)     return(0);
			else         break;
		}
		else
			return(-1);
	}
	return(n);
} 
void createpipe(int k,struct sarray *stra)
{
	if(pipe(stra[k].pipefd)<0){
		perror("pipe error");
		exit(0);
	}
}
int writefile(char *filename)
{
	int numbytes;
	FILE *fp;

	char buff[BUFSIZE];

	if ( (fp = fopen(filename, "wt")) == NULL )
			perror("can't open file");

	if ((numbytes = read(STDIN_FILENO, buff, sizeof(buff))) < 0)
		perror("Error");
	fputs(buff,fp);
	fclose(fp);
	return 0;
} 
